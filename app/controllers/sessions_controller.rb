class SessionsController < ApplicationController
  before_action :session_params, only: :create
  before_action :logged_user, only: [:new, :create]
  
  def new
  end
  
  
  def create
    user = User.find_by(email: session_params[:email])
    
    if user && user.authenticate(session_params[:password])
      log_in(user)
      redirect_to user_path(user)
    else
      render :new
    end
  end
  
  
  def destroy
    log_out
    redirect_to login_path
  end
  
  
  private
  
  def session_params
    @session_params = params.require(:session).permit(:email, :password)
  end
end